import hashlib
import timeit
s='hello'
def md5():
    
    (hashlib.md5(s.encode()).hexdigest())
    
t=timeit.timeit("md5()", setup="from __main__ import md5")
print(t)