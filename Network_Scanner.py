import scapy.all as scapy
import argparse
"""
def scan(ip):
    scapy.arping(ip)
scan("192.168.64.133/24")
"""
def get_arguments():
    parser=argparse.ArgumentParser()
    parser.add_argument("-t","--target",dest="target",help="Enter the range of ip ")
    options=parser.parse_args()
    return(options.target)
def arp_packet(ip):
    arp_req=scapy.ARP(pdst=ip)#or use this arp_req.pdst=ip
#    arp_req.show()
    broadcst=scapy.Ether(dst="ff:ff:ff:ff:ff:ff")#creating ethernet frame
#    broadcst.show()
    arp_req_broadcast=broadcst/arp_req
#    print(arp_req_broadcast.summary())
#    arp_req_broadcast.show()
    ans_list=scapy.srp(arp_req_broadcast,timeout=1)[0]
    print("IP\t\t\t\t\tMAC")
    client_list=list()
    for i in ans_list:
        print(i[1].psrc+"\t\t\t"+i[1].hwsrc)#ip
        dic={"ip":i[1].psrc,"mac":i[1].hwsrc}
        client_list.append(dic)#creating list of dictionaries
    return(client_list)
#        print(i[1].hwsrc)#mac

#    print(broadcst.summary())
#   scapy.ls(broadcast)
addr=input("Enter the network address!\n")
arp_packet(addr)
