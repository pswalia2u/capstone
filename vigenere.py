def encrypt():
    plaintext=input("Enter the plain text!\n")
    key=input("Enter the key!\n")
    key_length = len(key)
    key_as_int = [ord(i) for i in key]
    plaintext_int = [ord(i) for i in plaintext]
    ciphertext = ''
    for i in range(len(plaintext_int)):
        value = (plaintext_int[i] + key_as_int[i % key_length]) % 26
        ciphertext += chr(value + 65)
    print("Encrypted Text:",ciphertext)

def decrypt():
    ciphertext=input("Enter the cipher text!\n")
    key=input("Enter the Key!\n")
    key_length = len(key)
    key_as_int = [ord(i) for i in key]
    ciphertext_int = [ord(i) for i in ciphertext]
    plaintext = ''
    for i in range(len(ciphertext_int)):
        value = (ciphertext_int[i] - key_as_int[i % key_length]) % 26
        plaintext += chr(value + 65)
    print("Decrypted Text:",plaintext)
def Vigenere():
    n=input("Enter 1 for encryption and 2 for decryption!\n")
    if(n=='1'):
        encrypt()
    elif(n=='2'):
        decrypt()