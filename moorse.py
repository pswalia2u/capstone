
# Python program to implement Morse Code Translator 
  
''' 
VARIABLE KEY 
'cipher' -> 'stores the morse translated form of the english string' 
'decipher' -> 'stores the english translated form of the morse string' 
'citext' -> 'stores morse code of a single character' 
'i' -> 'keeps count of the spaces between morse characters' 
'message' -> 'stores the string to be encoded or decoded' 
'''
  
# Dictionary representing the morse code chart 
MORSE_CODE_DICT = { 'A':'.-', 'B':'-...', 
                    'C':'-.-.', 'D':'-..', 'E':'.', 
                    'F':'..-.', 'G':'--.', 'H':'....', 
                    'I':'..', 'J':'.---', 'K':'-.-', 
                    'L':'.-..', 'M':'--', 'N':'-.', 
                    'O':'---', 'P':'.--.', 'Q':'--.-', 
                    'R':'.-.', 'S':'...', 'T':'-', 
                    'U':'..-', 'V':'...-', 'W':'.--', 
                    'X':'-..-', 'Y':'-.--', 'Z':'--..', 
                    '1':'.----', '2':'..---', '3':'...--', 
                    '4':'....-', '5':'.....', '6':'-....', 
                    '7':'--...', '8':'---..', '9':'----.', 
                    '0':'-----', ', ':'--..--', '.':'.-.-.-', 
                    '?':'..--..', '/':'-..-.', '-':'-....-', 
                    '(':'-.--.', ')':'-.--.-'} 
  

def en():
    message=input("Enter the plain text!\n")
    message=message.upper()
    cipher = '' 
    for letter in message: 
        if letter != ' ': 

            cipher += MORSE_CODE_DICT[letter] + ' '
        else: 
         
            cipher += ' '
  
    print("Encoded Text:",cipher) 
  
 
def dec(): 

    message=input("Enter the cipher text!\n")
    
    message += ' '
  
    decipher = '' 
    citext = '' 
    for letter in message: 
  
         
        if (letter != ' '): 
  
            
            i = 0
  
            citext += letter 
  
         
        else: 
            i += 1
  
             
            if i == 2 : 
  
                 
                decipher += ' '
            else: 
  
                 
                decipher += list(MORSE_CODE_DICT.keys())[list(MORSE_CODE_DICT 
                .values()).index(citext)] 
                citext = '' 
  
    print("Decoded Text:",decipher) 
  
 

def morse():
    n=input("Enter 1 for encoding and 2 for decoding!\n")
    if(n=='1'):
        en()
    elif(n=='2'):
        dec()
    

