
def generate_keys(key_64bits):
 round_keys = list() 
 pc1_out = apply_PC1(PC1,key_64bits) 
 L0,R0 = split_in_half(pc1_out)
 for roundnumber in range(16):
  newL = circular_left_shift(L0,round_shifts[roundnumber])
  newR = circular_left_shift(R0,round_shifts[roundnumber])
  roundkey = apply_PC2(PC2,newL+newR)
  round_keys.append(roundkey)
  L0 = newL
  R0 = newR
 return round_keys
round_shifts = [1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1]
key_64bits = "0001001100110100010101110111100110011011101111001101111111110001"
subkeys = generate_keys(key_64bits)
# The subkeys will have all 16 keys as Python list
print(subkeys)

HEX_to_Binary = {'0':'0000',
		 '1':'0001',
		 '2':'0010',
		 '3':'0011',
		 '4':'0100',
		 '5':'0101',
		 '6':'0110',
		 '7':'0111',
		 '8':'1000',
		 '9':'1001',
		 'A':'1010',
		 'B':'1011',
		 'C':'1100',
		 'D':'1101',
		 'E':'1110',
		 'F':'1111',
		}


def hexDigit_to_binary_bits(hex_digit):
	binary_4bits = HEX_to_Binary[hex_digit]
	return binary_4bits

def hexString_to_binary_bits1(hex_string):
	binary_bits = ""
	for hex_digit in hex_string:
		binary_bits += hexDigit_to_binary_bits(hex_digit)
	return binary_bits

def hexString_to_binary_bits2(hexdigits):
	binarydigits = ""
	for hexdigit in hexdigits:
		binarydigits += bin(int(hexdigit,16))[2:].zfill(4)
	return binarydigits
		
M = '0123456789ABCDEF' # plaintext in hexdecimal 16 digits so 64 bits 
print(hexString_to_binary_bits1(M))
# Output: 0000000100100011010001010110011110001001101010111100110111101111
print(hexString_to_binary_bits2(M))
# Output: 0000000100100011010001010110011110001001101010111100110111101111


#-----------------------------------Initial Permutation-----------------------------

def hexTobinary(hexdigits):
	binarydigits = ""
	for hexdigit in hexdigits:
		binarydigits += bin(int(hexdigit,16))[2:].zfill(4)
	return binarydigits



INITIAL_PERMUTATION_TABLE = ['58 ', '50 ', '42 ', '34 ', '26 ', '18 ', '10 ', '2',
			 '60 ', '52 ', '44 ', '36 ', '28 ', '20 ', '12 ', '4',
			 '62 ', '54 ', '46 ', '38 ', '30 ', '22 ', '14 ', '6', 
			'64 ', '56 ', '48 ', '40 ', '32 ', '24 ', '16 ', '8', 
			'57 ', '49 ', '41 ', '33 ', '25 ', '17 ', '9 ', '1',
			 '59 ', '51 ', '43 ', '35 ', '27 ', '19 ', '11 ', '3',
			 '61 ', '53 ', '45 ', '37 ', '29 ', '21 ', '13 ', '5',
			 '63 ', '55 ', '47 ', '39 ', '31 ', '23 ', '15 ', '7']

def apply_initial_p(P_TABLE, PLAINTEXT):
	permutated_M = ""
	for index in P_TABLE:
		permutated_M += PLAINTEXT[int(index)-1]
	return permutated_M

M = '0123456789ABCDEF'

plaintext = hexTobinary(M)
print(plaintext)
print(apply_initial_p(INITIAL_PERMUTATION_TABLE,plaintext))

## output:

# 0000000100100011010001010110011110001001101010111100110111101111
# 1100110000000000110011001111111111110000101010101111000010101010


#---------------------------Splitting the plaintext (L,R)---------------------
M = '0123456789ABCDEF'

plaintext = hexTobinary(M)
print(plaintext)
p_plaintext = apply_initial_p(INITIAL_PERMUTATION_TABLE,plaintext)
print(p_plaintext)
## output:

# 0000000100100011010001010110011110001001101010111100110111101111
# 1100110000000000110011001111111111110000101010101111000010101010

def spliHalf(binarybits):
	return binarybits[:32],binarybits[32:]

L0,R0 = spliHalf(p_plaintext)
print(L0,R0)

#Output
#'11001100000000001100110011111111' '11110000101010101111000010101010'


#------------------------------Inverse Permutation------------------------
INVERSE_PERMUTATION_TABLE = ['40 ', '8 ', '48 ', '16 ', '56 ', '24 ', '64 ', '32',
			     '39 ', '7 ', '47 ', '15 ', '55 ', '23 ', '63 ', '31',
			     '38 ', '6 ', '46 ', '14 ',  '54 ', '22 ', '62 ', '30',
			     '37 ', '5 ', '45 ', '13 ', '53 ', '21 ', '61 ', '29',
			     '36 ', '4 ', '44 ', '12 ', '52 ', '20 ', '60 ', '28',
			     '35 ', '3 ', '43 ', '11 ', '51 ', '19 ', '59 ', '27', 
			     '34 ', '2 ', '42 ', '10 ', '50 ', '18 ', '58 ', '26',
			     '33 ', '1 ', '41 ', '9 ', '49 ', '17 ', '57 ', '25']


def apply_initial_p(INVERSE_P_TABLE, ROUND_OUT):
	cipher = ""
	for index in INVERSE_P_TABLE:
		cipher += ROUND_OUT[int(index)-1]
	return cipher
# Suppose

R16 = '11001100000000001100110011111111'  
L16 = '11110000101010101111000010101010'

cipher = apply_initial_p(INVERSE_PERMUTATION_TABLE, R16+L16)

print(cipher,len(cipher))
# Ouptput
# 0000000100100011010001010110011110001001101010111100110111101111 64


#---------------------------------encryption----------------------------
def XOR(bits1,bits2):	
	xor_result = ""
	for index in range(len(bits1)):
		if bits1[index] == bits2[index]:
			xor_result += '0'
		else:
			xor_result += '1'
	return xor_result
# Generate round keys Part- 1 of blog post

roundkeys = generate_keys(key_64bits)
# Suppose initial R and L value is 
R = '11001100000000001100110011111111'  
L = '11110000101010101111000010101010'

for round in range(16):
	# functionF() is implemented in part 2
	newR = XOR(L,functionF(R, roundkeys[round]) )
	newL = R
	# Swapping the value for next round
	R = newR
	L = newL

cipher = apply_initial_p(INVERSE_PERMUTATION_TABLE, R+L)

print(cipher)


#-----------------------Decryption-----------------
M = '0123456789ABCDEF'
key ='12345678ABCEDFF9'

def DES_encrypt(message,key):
	cipher = ""
	# Convert hex digits to binary
	plaintext_bits = hexTobinary(message)
	key_bits = hexTobinary(key)
	# Generate rounds key
	roundkeys = generate_keys(key_bits)
	#### DES steps
	
	## initial permutation
	p_plaintext = apply_initial_p(INITIAL_PERMUTATION_TABLE,plaintext_bits)
	## split in tow half
	L,R = spliHalf(p_plaintext)
	## start rounds
	for round in range(16):
		newR = XOR(L,functionF(R, roundkeys[round]))
		newL = R
		R = newR
		L = newL
	cipher = apply_initial_p(INVERSE_PERMUTATION_TABLE, R+L)
	return cipher