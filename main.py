import integrit
import asci
import urlencode
import base
import rot
import ceaser
import hashing
import moorse
import argparse
import subprocess
import vigenere
import rsa
import en_rsa
import dec_rsa
from Crypto.PublicKey import RSA

def symmetric():
    n=input("\n1.Ceaser cipher(Historical)\t2.Rot-n \t3.Vigenere cipher!\n")
    if(n=='1'):        
        ceaser.Ceaser()
    elif(n=='2'):
        rot.rotn()
    elif(n=='3'):
        vigenere.Vigenere()

def asymmetric():
    n=input("Enter 1 for RSA!\n")
    if(n=='1'):
        key = RSA.generate(2048)
        print("Generating Private key!\n")
        rsa.privte_key(key)
        print("Generating Public key!\n")
        rsa.pub_key(key)
        en_rsa.rsa_enc()
        print("Decrypting!")
        dec_rsa.rsa_dec()

    
def Encryption():
    n=input("1.Symmetric Encryption(Private-key cryptography)\t2.Asymetric Encryption(Public-key cryptography)\n")
    if(n=='1'):
        symmetric()
    elif(n=='2'):
        asymmetric()

def Encoding():
    n=input("1.Base64\t2.Moorse Code\t3.URL encoding\t4.Text-ASCII\n")
    if(n=='1'):
        base.Base()
    elif(n=='2'):
        moorse.morse()
    elif(n=='3'):
        urlencode.urlencodd()
    elif(n=='4'):
        asci.encodd()

        
def main():
    parser=argparse.ArgumentParser()
    parser.add_argument("Action",help="I for file integrity checker,En for Encoding E for Encryption(Symmetric->Ceaser,Rotn,Base64,morse code,Vigenere Asymmetric->RSA) H for hashing(md5,sha1,sha224,sha256,sha384,sha512) ID (Hash Identification) N (Cybersecurity News) S (Network Scanner)")
    args=parser.parse_args()
    if(args.Action=='E'):
        Encryption()
    elif(args.Action=='H'):
        hashing.hashing()
        print("\n\nWanna checkout hash-identifier?\nPress Y(Yes)/N(No)")
        n=input()
        if((n=='Y')or(n=='yes')or(n=='y')):
            subprocess.call('python hash-id.py',shell=True)
        elif((n=='n')or(n=='no')or(n=='N')):
            print("\n\nGoodbye!")   
    elif(args.Action=='ID'):
        subprocess.call('python hash-id.py',shell=True)
    elif(args.Action=='N'):
        subprocess.call('xdg-open https://thehackernews.com',shell=True)    
    elif(args.Action=='S'):
        subprocess.call('python3 Network_Scanner.py',shell=True)
    elif(args.Action=='En'):
        Encoding()
    elif(args.Action=='I'):
        integrit.checker()

print("""                                                                                                                                                                                                                                                                                                
      ACRYP    DIACRYPTOM  IACRY  OMED  CRYPTOMEDI  RYPTOMEDIACR    MEDIAC   TOMED       OMEDI  RYPTOMEDIAC  PTOMEDI      OMEDIA        DIA         
    YPTOMEDIAC YPTOMEDIACR PTOM   ACRY   MEDIACRYPT MEDIACRYPTOM   ACRYPTOM  IACRY       ACRYP  MEDIACRYPTOM DIACRYPTO    ACRYPT       RYPTO        
   MED   RYPTO  DIA    TOM   ACR  TO     CRYP    DIACRY  OME  AC  PTOM  IACR   OMED      TOM    CRYP    DIAC YPTOMEDIAC    OMED        MEDIA        
  ACR     EDIA  YPTOMEDIAC    OM DIA     OMEDIACRYPT     ACR      DIA    TOM   ACRYP    DIAC     MED         EDIA   PTOM    CRY       ACRYPT        
  TOM           EDIACRYPT     ACRYP      ACRYPTOMED      TOM     RYPT    IACR  TOMED    Y TO     CRYPTO       YPT   DIAC   TOM        TOM DIA       
  IA            RYPTOMED       OME       TOMEDIACR       IA      MEDI    PTOM  IACRYP  ME IA     OMEDIAC      EDI    PTO    ACR      DIACRYPTO      
  PT            MEDIACRYP      ACR       IAC             PTO      RYP    DIA   PT MEDI CR PT     ACRYPTO      RYP   EDIA    TO       YPTOMEDIA      
  DI       ME   CRY   MED  C   TOM       PTO             DIA      MED    YPT   DI CRYPTO  DIA    TOM         OMED  CRYPT    IA      MEDI   YPTO     
  YPT     ACR   OME   CRYPTO   IAC       DIA             YPT      CRYP  MEDI   YPT MEDIA RYPT    IACR    MED ACRYPTOMEDI   YPTO     CRY     DIA     
   DIACRYPTOM  IACRY  OMEDIA  YPTOM     RYPTOM         OMEDIAC     MEDIACRY  OMEDIA RYP OMEDIA  YPTOMED ACRY TOMEDIACRY   MEDIAC  PTOMED   RYPTOM   
    PTOMEDI    PTOME   CRYP   EDIAC     MEDIAC         ACRYPTO      RYPTOM    CRYPT ME  ACRYPT  EDIACRYPTOME IACRYPTO     CRYPTOM DIACRY   MEDIAC   
""")
main()
  


