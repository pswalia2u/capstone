import hashlib
import timeit
def md5():
    
    print("Enter the string!\n")
    s=input()
    s=str.encode(s)
    hash_object = hashlib.md5(s)
    c=hash_object.hexdigest()
    print("Hash:",c)
    
def sHa1():
    
    s=input("Enter the input!\n")
    def make_sha1(s, encoding='utf-8'):
        return hashlib.sha1(s.encode(encoding)).hexdigest()
    
    print("Hash:",make_sha1(s, encoding='utf-8'))
        

def sHa224():
    
    s=input("Enter the input!\n")
    
    def make_sha224(s, encoding='utf-8'):
        return hashlib.sha224(s.encode(encoding)).hexdigest()
    
    print("Hash:",make_sha224(s, encoding='utf-8'))
      

def sHa256():
    
    s=input("Enter the input!\n")
    
    def make_sha256(s, encoding='utf-8'):
        return hashlib.sha256(s.encode(encoding)).hexdigest()
    
    print("Hash:",make_sha256(s, encoding='utf-8'))
      

def sHa384():
    
    s=input("Enter the input!\n")
    
    def make_sha384(s, encoding='utf-8'):
        return hashlib.sha384(s.encode(encoding)).hexdigest()
    
    print("Hash:",make_sha384(s, encoding='utf-8'))
      

def sHa512():
    
    s=input("Enter the input!\n")
    
    def make_sha512(s, encoding='utf-8'):
        return hashlib.sha512(s.encode(encoding)).hexdigest()
    
    print("Hash:",make_sha512(s, encoding='utf-8'))
      
#------------------------calc for time---------------------
s='hello'
def timmd5():
    
    (hashlib.md5(s.encode()).hexdigest())
def timsha1():
    
    (hashlib.sha1(s.encode()).hexdigest())
def timsha224():
    
    (hashlib.sha224(s.encode()).hexdigest())
def timsha256():
    
    (hashlib.sha256(s.encode()).hexdigest())
def timsha384():
    
    (hashlib.sha384(s.encode()).hexdigest())
def timsha512():
    
    (hashlib.sha512(s.encode()).hexdigest())


def hashing():
    print("Hashing becomes easy!\n\n1.md5\n2.sha1\n3.sha224\n4.sha256\n5.sha384\n6.sha512\n")
    n=input()
    if n=='1':
        md5()
        t=timeit.timeit("timmd5()", setup="from hashing import timmd5")
        print("Execution Time:",t)
        
    if n=='2':
        sHa1()
        t=timeit.timeit("timsha1()", setup="from hashing import timsha1")
        print("Execution Time:",t)

    if n=='3':
        sHa224()
        t=timeit.timeit("timsha224()", setup="from hashing import timsha224")
        print("Execution Time:",t)

    if n=='4':
        sHa256()
        t=timeit.timeit("timsha256()", setup="from hashing import timsha256")
        print("Execution Time:",t)
    
    if n=='5':
        sHa384()
        t=timeit.timeit("timsha384()", setup="from hashing import timsha384")
        print("Execution Time:",t)

    if n=='6':
        sHa512()   
        t=timeit.timeit("timsha512()", setup="from hashing import timsha512")
        print("Execution Time:",t)
    



    
        
	
