import hashlib
from functools import partial

def md5sum(filename):
    with open(filename, mode='rb') as f:
        d = hashlib.md5()
        for buf in iter(partial(f.read, 128), b''):
            d.update(buf)
    f.close()
    return d.hexdigest()


def sha1(filename):
    with open(filename, mode='rb') as f:
        d = hashlib.sha1()
        for buf in iter(partial(f.read, 160), b''):
            d.update(buf)
    f.close()
    return d.hexdigest()

def checker():
    n=input("For file 1:\nEnter the filename(if file is in same dir) or file path!\n")
    n1=input("For file 2:\nEnter the filename(if file is in same dir) or file path!\n")
    a=input("Enter 1. to use md5 or 2. for sha1\n")
    if(a=='1'):
        if(md5sum(n)==md5sum(n1)):
            print("Files are same!")
        else:
            print("Files are different!")
    if(a=='2'):
        if(sha1(n)==sha1(n1)):
            print("Files are same!")
        else:
            print("Files are different!")




